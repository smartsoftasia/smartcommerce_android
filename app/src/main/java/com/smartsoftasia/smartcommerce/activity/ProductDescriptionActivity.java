package com.smartsoftasia.smartcommerce.activity;

import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.smartsoftasia.smartcommerce.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ProductDescriptionActivity extends ActionBarActivity {
    
    public static final String ARG_PRODUCT_DESCRIPTION = "arg_product_description";
    public static final String EXTRA_PRODUCT_DESCRIPTION = "extra_product_description";

    @InjectView(R.id.textView_fragment_product_description)
    protected TextView vDescription;
    
    private String mProductDescrption;
    private Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_description);
        setActionBar();


        mProductDescrption = getIntent().hasExtra(ARG_PRODUCT_DESCRIPTION) ?
                (String) getIntent().getStringExtra(ARG_PRODUCT_DESCRIPTION) : "";

        ButterKnife.inject(this);
        vDescription.setText(mProductDescrption);
        ViewCompat.setTransitionName(vDescription, EXTRA_PRODUCT_DESCRIPTION);
        
        
        
    }
    
    private void setActionBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
        }
        if(getSupportActionBar()!=null){
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_description, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
