package com.smartsoftasia.smartcommerce.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.view.Window;

import com.smartsoftasia.smartcommerce.R;
import com.smartsoftasia.smartcommerce.fragment.ProductFragment;
import com.smartsoftasia.smartcommerce.recycle.ProductAdapter;
import com.smartsoftasia.spreeandroid.model.SpreeProduct;

public class ProductActivity extends ActionBarActivity implements ProductFragment.OnFragmentInteractionListener, View.OnClickListener {

    public static final String ARG_PRODUCT = "product";


    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);




        if (savedInstanceState == null) {
            SpreeProduct product = getIntent().hasExtra(ARG_PRODUCT) ? (SpreeProduct) getIntent().getParcelableExtra
                    (ARG_PRODUCT) : null;
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, ProductFragment.newInstance(product))
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == android.R.id.home){
            finish();
        }

            return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(String productDescription, View view) {
        Intent intent = new Intent(this, ProductDescriptionActivity.class);
        intent.putExtra(ProductDescriptionActivity.ARG_PRODUCT_DESCRIPTION, productDescription);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, view, ProductDescriptionActivity.EXTRA_PRODUCT_DESCRIPTION);
        ActivityCompat.startActivity(this, intent,
                options.toBundle());
    }

    @Override
    public void onClick(View view) {
        if (view == null) return;
    }
}
