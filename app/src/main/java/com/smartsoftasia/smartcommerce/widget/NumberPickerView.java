package com.smartsoftasia.smartcommerce.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.smartsoftasia.smartcommerce.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by gregoire barret on 2/12/15.
 * For SmartCommerce project.
 */
public class NumberPickerView extends LinearLayout {
    public static final String TAG = "NumberPickerView";

    @InjectView(R.id.button_add)
    protected ImageButton vPlus;
    @InjectView(R.id.button_min)
    protected ImageButton vMinus;
    @InjectView(R.id.textView_number_picker_quantity)
    protected TextView vQuantity;


    private int mQuantity = 0;
    private Integer mMinQuantity;
    private Integer mMaxQuantity;
    
    private OnQuantityChange mQuantityChangeListener;

    public NumberPickerView(Context context) {
        super(context);
        setupViewItems(context);
    }

    public NumberPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupViewItems(context);
    }

    public NumberPickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupViewItems(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public NumberPickerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setupViewItems(context);
    }


    private void setupViewItems(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.number_picker_view, this);
        ButterKnife.inject(this, rootView);
        vQuantity.setText(String.valueOf(mQuantity));
    }

    public void setQuantity(int quantity){
        this.mQuantity = quantity;
        vQuantity.setText(String.valueOf(mQuantity));
        
    }
    
    public void setOnQuantityChangeListener(OnQuantityChange onQuantityChangeListener){
        this.mQuantityChangeListener = onQuantityChangeListener;
        
    }
    
    
    public void setRange(int min, int max){
        mMinQuantity = min;
        mMaxQuantity = max;
        if(mQuantity < mMinQuantity) mQuantity = mMinQuantity;
        else if(mQuantity > mMinQuantity) mQuantity = mMaxQuantity;
        
    }
    
    public interface OnQuantityChange{
        public void onQuantityChange(int quantity);
        
    }
    
    @OnClick(R.id.button_add)
    public void onAddClick(){
        if(mMaxQuantity!=null && mQuantity >= mMaxQuantity){
            mQuantity = mMaxQuantity;
        }else{
            mQuantity++;
        }
        vQuantity.setText(String.valueOf(mQuantity));
        if(mQuantityChangeListener!=null)mQuantityChangeListener.onQuantityChange(mQuantity);
        
    }

    @OnClick(R.id.button_min)
    public void onMinClick(){
        if(mMinQuantity!=null && mQuantity <= mMinQuantity){
            mQuantity = mMinQuantity;
        }else{
            mQuantity--;
        }
        vQuantity.setText(String.valueOf(mQuantity));
        if(mQuantityChangeListener!=null)mQuantityChangeListener.onQuantityChange(mQuantity);
        
    }
}
