package com.smartsoftasia.smartcommerce.core;

import android.app.Application;

import com.smartsoftasia.bigglibrary.imageView.ImageDownloader;
import com.smartsoftasia.spreeandroid.SpreeDroid;

/**
 * Created by gregoire barret on 2/11/15.
 * For SmartCommerce project.
 */
public class SmartCommerce extends Application {
    public static final String TAG = "SmartCommerce";

    @Override
    public void onCreate() {
        super.onCreate();

        SpreeDroid.getInstance()
                .setServerUrl("http://192.168.1.116:3000")
                .setUserToken("64a9dd4217e06815cf57eb4b007e92b1f299c9f8b016a016")
                .setContext(getApplicationContext());
        ImageDownloader.init(getApplicationContext());

    }
    
}
