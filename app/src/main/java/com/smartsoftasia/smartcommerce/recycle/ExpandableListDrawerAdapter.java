package com.smartsoftasia.smartcommerce.recycle;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.smartsoftasia.bigglibrary.widget.AnimatedExpandableListView;
import com.smartsoftasia.smartcommerce.R;
import com.smartsoftasia.spreeandroid.model.SpreeTaxon;
import com.smartsoftasia.spreeandroid.model.SpreeTaxonomy;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by gregoire barret on 2/11/15.
 * For SmartCommerce project.
 */
public class ExpandableListDrawerAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
    public static final String TAG = "ExpandableListDrawerAdapter";

    private Context _context;
    private List<SpreeTaxonomy> _listDataHeader; // header titles
    // child data in format of header title, child title
    private LinkedHashMap<SpreeTaxonomy, List<SpreeTaxon>> _listDataChild;

    private int lastPosition = -1;

    public ExpandableListDrawerAdapter(Context context, List<SpreeTaxonomy> listDataHeader,
                                       LinkedHashMap<SpreeTaxonomy, List<SpreeTaxon>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = ((SpreeTaxon) getChild(groupPosition, childPosition)).getName();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_adapter, null);
        }
        

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.textView_drawer_adapter);

        txtListChild.setText(childText);

        return convertView;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = ((SpreeTaxonomy) getGroup(groupPosition)).getName();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_adapter_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.textView_drawer_adapter_group);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
    
    
}
