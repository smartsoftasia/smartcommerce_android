package com.smartsoftasia.smartcommerce.recycle;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smartsoftasia.bigglibrary.Adapter.IRecycleAdapter;
import com.smartsoftasia.bigglibrary.Adapter.IRecycleEvent;
import com.smartsoftasia.bigglibrary.helper.CollectionHelper;
import com.smartsoftasia.bigglibrary.imageView.GBImageView;
import com.smartsoftasia.smartcommerce.R;
import com.smartsoftasia.spreeandroid.helper.Validator;
import com.smartsoftasia.spreeandroid.model.SpreeProduct;
import java.util.Collection;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by gregoire barret on 2/10/15.
 * For SmartCommerce project.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> implements 
        IRecycleAdapter<SpreeProduct> {
    public static final String TAG = "ProductAdapter";
    
    private List<SpreeProduct> products;
    private IRecycleEvent.OnRecycleViewClick recycleClick;


    public ProductAdapter(List<SpreeProduct> products) {
        this.products = products;
    }

    @Override
    public ProductAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.adapter_product, parent, false);

        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ProductViewHolder holder, int position) {
        SpreeProduct product = products.get(position);

        try {
            String imageUrl = product.getMaster().getImages().get(0).getProductFullUrl();
            holder.vImage.downloadImageFromURL(imageUrl);
        }catch (NullPointerException e){
            holder.vImage.setImageResource(R.drawable.smartsoftasia_logo);
        }

        holder.vTitle.setText(Validator.validate(product.getName()));
        holder.vContent.setText(Validator.validate(product.getDescription()));
        holder.vPrice.setText(Validator.validate(product.getPrice()));
        holder.vPromotion.setText(Validator.validate(product.getDisplayPrice()));
        
        if(product.getMaster()!=null 
                && Validator.isValid(product.getMaster().getCostPrice())) {
            holder.vDiscount.setVisibility(View.VISIBLE);
            holder.vDiscount.setText(Validator.validate(product.getMaster().getCostPrice()));
        }else{
            holder.vDiscount.setVisibility(View.INVISIBLE);
        }

        holder.setOnClickListener(recycleClick);

    }
    
    public void setOnClickListener(IRecycleEvent.OnRecycleViewClick recycleClick){
        this.recycleClick = recycleClick;
    }

    
    @Override
    public int getItemCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }


    @Override
    public void appendItems(Collection<SpreeProduct> items) {
        int first = this.products.size();
        this.products.addAll(items);
        //notifyItemRangeInserted(first-1, items.size());
        notifyDataSetChanged();
    }

    @Override
    public void appendItem(SpreeProduct item) {
        this.products.add(item);
        notifyItemInserted(products.size());
    }

    @Override
    public void clear() {
        this.products.clear();
        notifyDataSetChanged();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @InjectView(R.id.imageView_adapter_product)
        protected GBImageView vImage;
        @InjectView(R.id.textView_adapter_product_title)
        protected TextView vTitle;
        @InjectView(R.id.textView_adapter_product_content)
        protected TextView vContent;
        @InjectView(R.id.textView_adapter_product_price)
        protected TextView vPrice;
        @InjectView(R.id.textView_adapter_product_promotionPrice)
        protected TextView vPromotion;
        @InjectView(R.id.textView_adapter_product_discount)
        protected TextView vDiscount;

        private IRecycleEvent.OnRecycleViewClick recycleClick;
        
        public ProductViewHolder(View v) {
            super(v);
            ButterKnife.inject(this,v);
            v.setOnClickListener(this);
        }
        
        public void setOnClickListener(IRecycleEvent.OnRecycleViewClick recycleClick){
            this.recycleClick = recycleClick;
            
        }

        @Override
        public void onClick(View view) {
            if(recycleClick!=null)recycleClick.onRecycleViewClick(getPosition(), vTitle);
        }
    }
}
