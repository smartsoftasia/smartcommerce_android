package com.smartsoftasia.smartcommerce.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.smartsoftasia.bigglibrary.Adapter.IRecycleEvent;
import com.smartsoftasia.bigglibrary.imageView.ImageDownloader;
import com.smartsoftasia.smartcommerce.R;
import com.smartsoftasia.smartcommerce.recycle.ProductAdapter;
import com.smartsoftasia.spreeandroid.SpreeDroid;
import com.smartsoftasia.spreeandroid.model.SpreeProduct;
import com.smartsoftasia.spreeandroid.volley.VolleyError;
import com.smartsoftasia.spreeandroid.volley.toolbox.VolleyPaginate;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductsFragment extends Fragment implements IRecycleEvent.OnRecycleViewClick, SwipeRefreshLayout.OnRefreshListener {
    private static final String ARG_TAXON_ID = "paramTaxonId";

    private Integer mTaxonId;


    @InjectView(R.id.recycler_fragment_product_cardList)
    protected RecyclerView vRecycler; 
    @InjectView(R.id.progressBar_fragment_products)
    protected ProgressBarCircularIndeterminate vProgressBar;
    @InjectView(R.id.textView_fragment_product_no_items)
    protected TextView vNoProduct;
    @InjectView(R.id.swipe_container)
    protected SwipeRefreshLayout vSwipeRefreshLayout;
    
    private VolleyPaginate mPaginate;
    
    private ProductAdapter mAdapter;
    private OnFragmentInteractionListener mListener;


    public static ProductsFragment newInstance(Integer taxonId) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        if(taxonId!=null)args.putInt(ARG_TAXON_ID, taxonId);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().containsKey(ARG_TAXON_ID))mTaxonId = getArguments().getInt(ARG_TAXON_ID);
        }else {
            mTaxonId = null;
        }


        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_products, container, false);

        ButterKnife.inject(this,root);

        initView();
        getNewProducts();
        
        return root;
    }
    
    private void initView(){
        vRecycler.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);
        vRecycler.setLayoutManager(layoutManager);
        mAdapter = new ProductAdapter(new ArrayList<SpreeProduct>());
        mAdapter.setOnClickListener(this);
        vRecycler.setAdapter(mAdapter);
        
        vSwipeRefreshLayout.setOnRefreshListener(this);
        vSwipeRefreshLayout.setColorSchemeResources(R.color.primary);
        
        setVisibility(true, 1);
        
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    
    private void setVisibility(Boolean enable, int count){
        int visibility = enable? View.VISIBLE: View.GONE;
        vProgressBar.setVisibility(visibility);

        if(count<=0)vNoProduct.setVisibility(View.VISIBLE);
        else vNoProduct.setVisibility(View.GONE);
        
    }

    @Override
    public void onRecycleViewClick(int position, View view) {
        if(mListener!=null)mListener.onFragmentInteraction((SpreeProduct)mAdapter.getItem(position), view);
    }

    @Override
    public void onRefresh() {
        getNewProducts();
    }
    
    private void getNewProducts(){
        SpreeProduct.Index(mPaginate, mTaxonId, new SpreeProduct.IProduct.OnIndex() {
            @Override
            public void onSuccess(List<SpreeProduct> spreeProducts, VolleyPaginate paginate) {
                mPaginate = paginate;
                vSwipeRefreshLayout.setRefreshing(false);
                setVisibility(false, spreeProducts.size());
                
                
                if(mPaginate.getCurrentPageInt()<=1)((ProductAdapter)vRecycler.getAdapter()).clear();
                ((ProductAdapter)vRecycler.getAdapter()).appendItems(spreeProducts);
            }
            @Override
            public void onFail(VolleyError error) {
                vSwipeRefreshLayout.setRefreshing(false);
                setVisibility(false, 0);
            }
        });
        
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(SpreeProduct product, View transitionView);
    }

}
