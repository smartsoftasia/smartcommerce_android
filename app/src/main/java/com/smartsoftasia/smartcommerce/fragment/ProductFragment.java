package com.smartsoftasia.smartcommerce.fragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.smartsoftasia.imageslider.PlaceDetailsFragment;
import com.smartsoftasia.smartcommerce.R;
import com.smartsoftasia.smartcommerce.widget.NumberPickerView;
import com.smartsoftasia.spreeandroid.model.SpreeProduct;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductFragment extends Fragment implements NumberPickerView.OnQuantityChange {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PRODUCT = "product";

    public static final String EXTRA_TITLE = "ProductFragment:title";

    private SpreeProduct mProduct;

    private OnFragmentInteractionListener mListener;
    
    protected PlaceDetailsFragment fSlider;
    
    @InjectView(R.id.textView_fragment_product_title)
    protected TextView vProductTitle;
    @InjectView(R.id.textView_fragment_product_price)
    protected TextView vProductPrice;
    @InjectView(R.id.textView_fragment_product_description)
    protected TextView vProductDescription;
    @InjectView(R.id.textView_fragment_product_total_price)
    protected TextView vProductTotalPrice;
    @InjectView(R.id.numberPicker_fragment_product)
    protected NumberPickerView vNumberPicker;

    public static ProductFragment newInstance(SpreeProduct product) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, product);
        fragment.setArguments(args);
        return fragment;
    }

    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProduct = getArguments().getParcelable(ARG_PRODUCT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.inject(this,root);
        
        
        fSlider = PlaceDetailsFragment.newInstance(mProduct.getMaster().getImagesArray());
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_slider, fSlider);
        ft.commit();

        ViewCompat.setTransitionName(vProductTitle, EXTRA_TITLE);

        initView();
        
        return root;
    }

    
    private void initView(){
        vProductTitle.setText(mProduct.getName());
        vProductDescription.setText(mProduct.getDescription());
        vProductPrice.setText(mProduct.getDisplayPrice());
        vNumberPicker.setRange(0,20);
        vNumberPicker.setOnQuantityChangeListener(this);
        
        
    }
    
    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.button_fragment_product_viewMore)
    public void onButtonViewMorePressed() {
        if (mListener != null) {
            mListener.onFragmentInteraction(mProduct.getDescription(), vProductDescription);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onQuantityChange(int quantity) {
        vProductTotalPrice.setText(String.valueOf(quantity * Float.valueOf(mProduct.getPrice())));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(String productDescription, View view);
    }

}
