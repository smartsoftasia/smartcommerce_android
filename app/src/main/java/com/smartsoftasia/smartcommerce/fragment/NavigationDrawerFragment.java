package com.smartsoftasia.smartcommerce.fragment;

import android.app.ExpandableListActivity;
import android.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.smartsoftasia.bigglibrary.widget.AnimatedExpandableListView;
import com.smartsoftasia.smartcommerce.R;
import com.smartsoftasia.smartcommerce.recycle.ExpandableListDrawerAdapter;
import com.smartsoftasia.spreeandroid.model.SpreeTaxon;
import com.smartsoftasia.spreeandroid.model.SpreeTaxonomy;
import com.smartsoftasia.spreeandroid.volley.VolleyError;
import com.smartsoftasia.spreeandroid.volley.toolbox.VolleyPaginate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String STATE_SELECTED_TAXON_ID = "selected_navigation_drawer_taxon_id";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    private DrawerLayout mDrawerLayout;
    private View mFragmentContainerView;

    
    private int mCurrentSelectedPosition = 0;
    private int mCurrentSelectedTaxonId = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;



    ExpandableListDrawerAdapter listAdapter;
    @InjectView(R.id.expandListView_navigation_drawer)
    protected AnimatedExpandableListView vExpList;
    
    private List<SpreeTaxonomy> mListDataHeader;
    private LinkedHashMap<SpreeTaxonomy, List<SpreeTaxon>> mListDataChild;

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mCurrentSelectedTaxonId = savedInstanceState.getInt(STATE_SELECTED_TAXON_ID);
            mFromSavedInstanceState = true;
        }

        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition,mCurrentSelectedTaxonId);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_navigation_drawer, container);

        ButterKnife.inject(this, root);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListDrawerAdapter(getActivity(), mListDataHeader, mListDataChild);

        // setting list adapter
        vExpList.setAdapter(listAdapter);

        vExpList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                if (vExpList.isGroupExpanded(groupPosition)) {
                    vExpList.collapseGroupWithAnimation(groupPosition);
                } else {
                    vExpList.expandGroupWithAnimation(groupPosition);
                }
                return true;
            }
        });
        vExpList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition,
                                        long l) {
                int index = expandableListView.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                expandableListView.setItemChecked(index, true);
                SpreeTaxon taxon = (new ArrayList<>(mListDataChild.values())).get(groupPosition).get
                        (childPosition);
                selectItem(childPosition, taxon.getId());
                return true;
            }
        });

        return root;
    }


    /*
     * Preparing the list data
     */
    private void prepareListData() {
        mListDataHeader = new ArrayList<SpreeTaxonomy>();
        mListDataChild = new LinkedHashMap<SpreeTaxonomy, List<SpreeTaxon>>();
        
        SpreeTaxonomy.Index(null, new SpreeTaxonomy.ITaxonomy.OnIndex() {
            @Override
            public void onSuccess(List<SpreeTaxonomy> spreeTaxonomies, VolleyPaginate paginate) {
                // Adding child data
                for (int i=0 ; i<spreeTaxonomies.size(); i++){
                    mListDataHeader.add(spreeTaxonomies.get(i));
                    List<SpreeTaxon> taxonNames = new ArrayList<SpreeTaxon>();
                    for (SpreeTaxon spreeTaxon : spreeTaxonomies.get(i).getRoot().getTaxons()){
                        taxonNames.add(spreeTaxon);
                        mListDataChild.put(mListDataHeader.get(i), taxonNames);
                    }

                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFail(VolleyError error) {

            }
        });
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.START);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener


    }

    private void selectItem(int position, int taxonId) {
        mCurrentSelectedPosition = position;

        if(vExpList!=null)
            vExpList.setItemChecked(position,true);
        
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(taxonId);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
//        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
/*        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return ((ActionBarActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int taxonId);
    }
}
